title POST: /login

actor User
participant /login
participant bookstoreDB

User->/login:send username,password
alt handle error

activate /login
/login->/login: encode password
/login->bookstoreDB:send username,encode password
activate bookstoreDB
bookstoreDB->bookstoreDB:find user_ID from username,password in User Table

alt Found User
/login<-bookstoreDB: send found
/login<-/login:create JWT (insert user_ID in payload)
User<-/login: Response: 200 OK and send JWT
else
/login<-bookstoreDB: not found
User<-/login: Response: 404 Not found
end

else found error
User<-/login: Response: 500 Error
end