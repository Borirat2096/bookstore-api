title GET: /users

participant fronend
participant /users
participant bookstoreDB

fronend->/users:send JWT
alt handle error

activate /users
/users->/users: check JWT
alt check JWT Signature Verified
/users->bookstoreDB: send user_ID from JWT
activate bookstoreDB
bookstoreDB->bookstoreDB:find username, surename and date_of_birth in User table
/users<-bookstoreDB: result from DB
alt found user data
/users->bookstoreDB: send user_ID from JWT
bookstoreDB->bookstoreDB:find order in OrderID
/users<-bookstoreDB: result from DB
alt found order
/users->/users: take result from db to be array, the array for books attribute
fronend<-/users:Response 200 OK and send name, surename, date_of_birth and books
else 
fronend<-/users:Response 200 OK and send name, surename, date_of_birth and books(data in this attribute is [])
end
else 
fronend<-/users:Response 404 UserData not found
end
else 
fronend<-/users:Response 401 JWT Invalid Signature
end

else found error
fronend<-/users:Response 500 Error
end
